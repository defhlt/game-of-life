package io.github.mlatu.gameoflife

/**
 * Created by Artem Kholodnyi on 6/6/16.
 */

data class Cell (
    val x: Int,
    val y: Int
)