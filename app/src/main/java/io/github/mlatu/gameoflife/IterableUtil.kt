package io.github.mlatu.gameoflife

import java.util.*

/**
 * Created by Artem Kholodnyi on 6/6/16.
 */

fun <I> Iterable<I>.frequencies(): Map<I, Int> {
     val map = HashMap<I, Int>()

     for(i in this) {
          map.put(i, (map[i] ?: 0) + 1)
     }

     return map
}
