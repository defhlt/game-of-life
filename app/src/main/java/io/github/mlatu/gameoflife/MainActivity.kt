package io.github.mlatu.gameoflife

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.SurfaceHolder
import io.github.mlatu.gameoflife.R.layout.activity_main
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Created by Artem Kholodnyi on 6/6/16.
 */

class MainActivity : AppCompatActivity(), SurfaceHolder.Callback, Runnable {
    companion object {
        const val WORLD_SIZE = 32
    }

    private var mHolder: SurfaceHolder? = null
    private val paint = Paint()
    @Suppress("unused")
    val blinker = setOf(Cell(1, 2), Cell(2, 2), Cell(3, 2))
    @Suppress("unused")
    val glider = setOf(Cell(1, 0), Cell(2, 1), Cell(0, 2), Cell(1, 2), Cell(2, 2))
    var setOfCells: Set<Cell> = glider
    var touchCells: Set<Cell> = emptySet()

    init {
        paint.color = Color.YELLOW
        paint.style = Paint.Style.FILL
    }

    override fun run() {
        while (true) {
            if (mHolder?.surface?.isValid != null) {
                val canvas = mHolder?.lockCanvas();

                setOfCells = step(setOfCells) + touchCells
                touchCells = emptySet()

                synchronized(surface) {
                    onDraw(canvas);
                }

                Thread.sleep(200)
                mHolder?.unlockCanvasAndPost(canvas);
            }
        }
    }

    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {

    }

    override fun onResume() {
        super.onResume()
        Thread(this).start()
        surface.setOnTouchListener { view, e ->
            touchCells += Cell(e.x.div(mSize).toInt(), e.y.div(mSize).toInt())
            true
        }
    }


    override fun surfaceCreated(holder: SurfaceHolder?) {
        mHolder = holder
    }

    private var mSize: Int = 0

    private fun onDraw(canvas: Canvas?) {
        canvas?.drawColor(Color.BLUE)

        mSize = canvas?.width?.div(WORLD_SIZE) ?: 0

        for (x in 0..WORLD_SIZE - 1) {
            for (y in 0..WORLD_SIZE - 1) {
                val cellAlive = setOfCells.contains(Cell(x, y))
                if (cellAlive) {
                    val rect = Rect(x * mSize, y * mSize, (x + 1) * mSize, (y + 1) * mSize)
                    canvas?.drawRect(rect, paint)
                }
            }
        }

    }

    override fun surfaceDestroyed(holder: SurfaceHolder?) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_main)

        surface.holder.addCallback(this)
    }
}
