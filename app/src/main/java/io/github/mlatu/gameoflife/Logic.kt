package io.github.mlatu.gameoflife

import java.util.*

fun moreNeighborhood(cell: Cell): List<Cell> {
    val list = ArrayList<Cell>()

    for (dx in -1..1) {
        for (dy in -1..1) {
            if (dx != 0 || dy != 0) {
                list += Cell(cell.x + dx, cell.y + dy)
            }
        }
    }

    return list
}

fun step(setOfCells: Set<Cell>): Set<Cell> {
    val result = HashSet<Cell>()

    for ((cell, count) in setOfCells.map { moreNeighborhood(it) }.flatten().frequencies()) {
        if (count == 3 || (count == 2 && setOfCells.contains(cell))) {
            result += cell
        }
    }

    return result
}

